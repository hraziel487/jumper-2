﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Coin::Start()
extern void Coin_Start_m514F3321444C25610980620A5AE926E8C5293C01 (void);
// 0x00000002 System.Void Coin::Update()
extern void Coin_Update_m22024328A66C145A574F4D5730574500A5C50761 (void);
// 0x00000003 System.Void Coin::OnTriggerEnter(UnityEngine.Collider)
extern void Coin_OnTriggerEnter_mEA9A278082B5DDE0204F3BD58089D40170EB6867 (void);
// 0x00000004 System.Void Coin::OnDestroy()
extern void Coin_OnDestroy_mB859C129A772310F9DEEF8D472DF749A35219C5A (void);
// 0x00000005 System.Void Coin::.ctor()
extern void Coin__ctor_mF977D2CE7F9F781D0E5B1226BD2757CC523DB637 (void);
// 0x00000006 System.Void Coin::.cctor()
extern void Coin__cctor_m9400D2A6AB8A4A2C53AF612C89B4AA35E54B5B28 (void);
// 0x00000007 System.Void Da?oYVida::Start()
extern void DaUF1oYVida_Start_mFE025EE2FC11D0DEBA66369D0959C3D479987650 (void);
// 0x00000008 System.Void Da?oYVida::OnTriggerStay(UnityEngine.Collider)
extern void DaUF1oYVida_OnTriggerStay_m86B58391731271C925F1482CCC9F8648E7D79A3F (void);
// 0x00000009 System.Void Da?oYVida::Update()
extern void DaUF1oYVida_Update_m889EE8D443A639BC6AA6AD387BC1EB8F75760C77 (void);
// 0x0000000A System.Void Da?oYVida::.ctor()
extern void DaUF1oYVida__ctor_mDA304D37C6E56BCAE898067F439BC7AA511DB533 (void);
// 0x0000000B System.Void GameOverManager::Start()
extern void GameOverManager_Start_mCF340FAB95187564C54942AD01D2A65BD005CEDD (void);
// 0x0000000C System.Void GameOverManager::Update()
extern void GameOverManager_Update_mCC4D30DD698BBDEAEE7EC313A9446B5457C356EF (void);
// 0x0000000D System.Void GameOverManager::.ctor()
extern void GameOverManager__ctor_m52207DE1B4AFE3EB17B968DC034DF9CD126BE15B (void);
// 0x0000000E System.Void InstruccionesCrontroller::GameInstruccions()
extern void InstruccionesCrontroller_GameInstruccions_mB25C39AA337C68EB05E6EB7CE6A54E9D4872F405 (void);
// 0x0000000F System.Void InstruccionesCrontroller::.ctor()
extern void InstruccionesCrontroller__ctor_m9B363FEAE2917D22E5E6ACD6223CF3851ADE259C (void);
// 0x00000010 System.Void Pausa::Awake()
extern void Pausa_Awake_m5504FB5E48790BE3B1CFB15578D7D0E57C30D548 (void);
// 0x00000011 System.Void Pausa::Update()
extern void Pausa_Update_mF8A56C9C51C5EB14A5B99AC1A6A1A9BFA94CCF02 (void);
// 0x00000012 System.Void Pausa::ContinuarJuego()
extern void Pausa_ContinuarJuego_m4E37F3E2C5FA21D035BE897DABCAC5E751B37DEF (void);
// 0x00000013 System.Void Pausa::SalirJuego()
extern void Pausa_SalirJuego_m77665A30D01636BCD5F62B0904C7A9D939D10988 (void);
// 0x00000014 System.Void Pausa::.ctor()
extern void Pausa__ctor_m3025D5274AD844EF4940CDD58A010F128CC3221C (void);
// 0x00000015 System.Void PlayerController::Start()
extern void PlayerController_Start_m9531F30EC892BDD1758A2EEC724E86EFBDA150A3 (void);
// 0x00000016 System.Void PlayerController::Update()
extern void PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794 (void);
// 0x00000017 System.Single PlayerController::ClampAngle(System.Single,System.Single,System.Single)
extern void PlayerController_ClampAngle_mA3D02618B3A3D6CB4A8492888DD15136F6A933A8 (void);
// 0x00000018 System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB (void);
// 0x00000019 System.Void Puntaje::Update()
extern void Puntaje_Update_mB92A0BC8A55C86BA9072402B1D3F39DD110D4D0E (void);
// 0x0000001A System.Void Puntaje::.ctor()
extern void Puntaje__ctor_mF7264D67AC446FDF706CAD045F5C904F169E5A6E (void);
// 0x0000001B System.Void ResetGame::Reiniciar()
extern void ResetGame_Reiniciar_mD3CF3B4CA15F9E232F75CB7E01CFD9B651E6E856 (void);
// 0x0000001C System.Void ResetGame::.ctor()
extern void ResetGame__ctor_mA0C4CE1B611652F508F46F519BC9DF8EDF3F64CF (void);
// 0x0000001D System.Void SaltoPersonaje::Start()
extern void SaltoPersonaje_Start_mB3531A28186F61C9103DAD8E749D74BAE9AAA922 (void);
// 0x0000001E System.Void SaltoPersonaje::Update()
extern void SaltoPersonaje_Update_mF379858286AE9FEDC5258237840076216EB92277 (void);
// 0x0000001F System.Void SaltoPersonaje::FixedUpdate()
extern void SaltoPersonaje_FixedUpdate_m4DC3345E61777E0E4B3433C77FDE1FE8318ED0D2 (void);
// 0x00000020 System.Void SaltoPersonaje::.ctor()
extern void SaltoPersonaje__ctor_m99B737BF9221BF4AB9C231CED19824229F501BEA (void);
// 0x00000021 System.Void VidaPlayer::Start()
extern void VidaPlayer_Start_mBA8C4F4B125B8E3A497FAD2AB475D55F00FAC632 (void);
// 0x00000022 System.Void VidaPlayer::Update()
extern void VidaPlayer_Update_m4D2D0248E26A6AC9FF039F246F0B0EB37AB41433 (void);
// 0x00000023 System.Void VidaPlayer::.ctor()
extern void VidaPlayer__ctor_mB26A3E2CC7C33CCFF85D25190EF32789B8831CCD (void);
// 0x00000024 System.Void Readme::.ctor()
extern void Readme__ctor_mF465410C5B2E598F2685E82CFCE1F42186AFF448 (void);
// 0x00000025 System.Void Readme/Section::.ctor()
extern void Section__ctor_mBAD5262A353BC071C61B8DB462A3D4D5AB5C7C4E (void);
// 0x00000026 System.Void UnityTemplateProjects.SimpleCameraController::OnEnable()
extern void SimpleCameraController_OnEnable_m2BA1F31BEDE84695933E86CF731059D6FAC2111C (void);
// 0x00000027 UnityEngine.Vector3 UnityTemplateProjects.SimpleCameraController::GetInputTranslationDirection()
extern void SimpleCameraController_GetInputTranslationDirection_m924AB4CEA66ADC4F1C63EAC21B660E0495765F4A (void);
// 0x00000028 System.Void UnityTemplateProjects.SimpleCameraController::Update()
extern void SimpleCameraController_Update_m0E587F33074BE72C54BA835B2217B0E8EC94F24A (void);
// 0x00000029 System.Single UnityTemplateProjects.SimpleCameraController::GetBoostFactor()
extern void SimpleCameraController_GetBoostFactor_m42B667ADEACC6F9CF0E4B6549A501609A15716AE (void);
// 0x0000002A UnityEngine.Vector2 UnityTemplateProjects.SimpleCameraController::GetInputLookRotation()
extern void SimpleCameraController_GetInputLookRotation_m4F945850C9036FAF5C3436AA42A1C3CB918E1BB9 (void);
// 0x0000002B System.Boolean UnityTemplateProjects.SimpleCameraController::IsBoostPressed()
extern void SimpleCameraController_IsBoostPressed_m26120B18155EBB68597EA52F392AD1E59F9A71E0 (void);
// 0x0000002C System.Boolean UnityTemplateProjects.SimpleCameraController::IsEscapePressed()
extern void SimpleCameraController_IsEscapePressed_mC694179281D2F4B20E831995751C70A26806488D (void);
// 0x0000002D System.Boolean UnityTemplateProjects.SimpleCameraController::IsCameraRotationAllowed()
extern void SimpleCameraController_IsCameraRotationAllowed_mABB9E26C1CC5C5643B305685F046B4B8493C7AFA (void);
// 0x0000002E System.Boolean UnityTemplateProjects.SimpleCameraController::IsRightMouseButtonDown()
extern void SimpleCameraController_IsRightMouseButtonDown_m0DE4F75C0A0AE963F333712F226000E0AE2DC32A (void);
// 0x0000002F System.Boolean UnityTemplateProjects.SimpleCameraController::IsRightMouseButtonUp()
extern void SimpleCameraController_IsRightMouseButtonUp_mEDE93C76DE7120043B15F390865340D1C5165CE5 (void);
// 0x00000030 System.Void UnityTemplateProjects.SimpleCameraController::.ctor()
extern void SimpleCameraController__ctor_mC6AE2509DDF461856450EC7DE0058A1687AB5C87 (void);
// 0x00000031 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::SetFromTransform(UnityEngine.Transform)
extern void CameraState_SetFromTransform_mAF13C515CFB1085295C01A870D93375E98F16647 (void);
// 0x00000032 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::Translate(UnityEngine.Vector3)
extern void CameraState_Translate_mB8F7239BD9DB70190E59D47D75DD125AD9AF3A96 (void);
// 0x00000033 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::LerpTowards(UnityTemplateProjects.SimpleCameraController/CameraState,System.Single,System.Single)
extern void CameraState_LerpTowards_mF2D4B962A677B281ED2F539A2FFF8A693FB9A326 (void);
// 0x00000034 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::UpdateTransform(UnityEngine.Transform)
extern void CameraState_UpdateTransform_mE653356FD34828D19ECB6793439A14C38F372410 (void);
// 0x00000035 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::.ctor()
extern void CameraState__ctor_m9C5338CABE70B8C73F8A4A08C1AFA1B33417DE9D (void);
static Il2CppMethodPointer s_methodPointers[53] = 
{
	Coin_Start_m514F3321444C25610980620A5AE926E8C5293C01,
	Coin_Update_m22024328A66C145A574F4D5730574500A5C50761,
	Coin_OnTriggerEnter_mEA9A278082B5DDE0204F3BD58089D40170EB6867,
	Coin_OnDestroy_mB859C129A772310F9DEEF8D472DF749A35219C5A,
	Coin__ctor_mF977D2CE7F9F781D0E5B1226BD2757CC523DB637,
	Coin__cctor_m9400D2A6AB8A4A2C53AF612C89B4AA35E54B5B28,
	DaUF1oYVida_Start_mFE025EE2FC11D0DEBA66369D0959C3D479987650,
	DaUF1oYVida_OnTriggerStay_m86B58391731271C925F1482CCC9F8648E7D79A3F,
	DaUF1oYVida_Update_m889EE8D443A639BC6AA6AD387BC1EB8F75760C77,
	DaUF1oYVida__ctor_mDA304D37C6E56BCAE898067F439BC7AA511DB533,
	GameOverManager_Start_mCF340FAB95187564C54942AD01D2A65BD005CEDD,
	GameOverManager_Update_mCC4D30DD698BBDEAEE7EC313A9446B5457C356EF,
	GameOverManager__ctor_m52207DE1B4AFE3EB17B968DC034DF9CD126BE15B,
	InstruccionesCrontroller_GameInstruccions_mB25C39AA337C68EB05E6EB7CE6A54E9D4872F405,
	InstruccionesCrontroller__ctor_m9B363FEAE2917D22E5E6ACD6223CF3851ADE259C,
	Pausa_Awake_m5504FB5E48790BE3B1CFB15578D7D0E57C30D548,
	Pausa_Update_mF8A56C9C51C5EB14A5B99AC1A6A1A9BFA94CCF02,
	Pausa_ContinuarJuego_m4E37F3E2C5FA21D035BE897DABCAC5E751B37DEF,
	Pausa_SalirJuego_m77665A30D01636BCD5F62B0904C7A9D939D10988,
	Pausa__ctor_m3025D5274AD844EF4940CDD58A010F128CC3221C,
	PlayerController_Start_m9531F30EC892BDD1758A2EEC724E86EFBDA150A3,
	PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794,
	PlayerController_ClampAngle_mA3D02618B3A3D6CB4A8492888DD15136F6A933A8,
	PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB,
	Puntaje_Update_mB92A0BC8A55C86BA9072402B1D3F39DD110D4D0E,
	Puntaje__ctor_mF7264D67AC446FDF706CAD045F5C904F169E5A6E,
	ResetGame_Reiniciar_mD3CF3B4CA15F9E232F75CB7E01CFD9B651E6E856,
	ResetGame__ctor_mA0C4CE1B611652F508F46F519BC9DF8EDF3F64CF,
	SaltoPersonaje_Start_mB3531A28186F61C9103DAD8E749D74BAE9AAA922,
	SaltoPersonaje_Update_mF379858286AE9FEDC5258237840076216EB92277,
	SaltoPersonaje_FixedUpdate_m4DC3345E61777E0E4B3433C77FDE1FE8318ED0D2,
	SaltoPersonaje__ctor_m99B737BF9221BF4AB9C231CED19824229F501BEA,
	VidaPlayer_Start_mBA8C4F4B125B8E3A497FAD2AB475D55F00FAC632,
	VidaPlayer_Update_m4D2D0248E26A6AC9FF039F246F0B0EB37AB41433,
	VidaPlayer__ctor_mB26A3E2CC7C33CCFF85D25190EF32789B8831CCD,
	Readme__ctor_mF465410C5B2E598F2685E82CFCE1F42186AFF448,
	Section__ctor_mBAD5262A353BC071C61B8DB462A3D4D5AB5C7C4E,
	SimpleCameraController_OnEnable_m2BA1F31BEDE84695933E86CF731059D6FAC2111C,
	SimpleCameraController_GetInputTranslationDirection_m924AB4CEA66ADC4F1C63EAC21B660E0495765F4A,
	SimpleCameraController_Update_m0E587F33074BE72C54BA835B2217B0E8EC94F24A,
	SimpleCameraController_GetBoostFactor_m42B667ADEACC6F9CF0E4B6549A501609A15716AE,
	SimpleCameraController_GetInputLookRotation_m4F945850C9036FAF5C3436AA42A1C3CB918E1BB9,
	SimpleCameraController_IsBoostPressed_m26120B18155EBB68597EA52F392AD1E59F9A71E0,
	SimpleCameraController_IsEscapePressed_mC694179281D2F4B20E831995751C70A26806488D,
	SimpleCameraController_IsCameraRotationAllowed_mABB9E26C1CC5C5643B305685F046B4B8493C7AFA,
	SimpleCameraController_IsRightMouseButtonDown_m0DE4F75C0A0AE963F333712F226000E0AE2DC32A,
	SimpleCameraController_IsRightMouseButtonUp_mEDE93C76DE7120043B15F390865340D1C5165CE5,
	SimpleCameraController__ctor_mC6AE2509DDF461856450EC7DE0058A1687AB5C87,
	CameraState_SetFromTransform_mAF13C515CFB1085295C01A870D93375E98F16647,
	CameraState_Translate_mB8F7239BD9DB70190E59D47D75DD125AD9AF3A96,
	CameraState_LerpTowards_mF2D4B962A677B281ED2F539A2FFF8A693FB9A326,
	CameraState_UpdateTransform_mE653356FD34828D19ECB6793439A14C38F372410,
	CameraState__ctor_m9C5338CABE70B8C73F8A4A08C1AFA1B33417DE9D,
};
static const int32_t s_InvokerIndices[53] = 
{
	2949,
	2949,
	2372,
	2949,
	2949,
	4648,
	2949,
	2372,
	2949,
	2949,
	2949,
	2949,
	2949,
	2949,
	2949,
	2949,
	2949,
	2949,
	2949,
	2949,
	2949,
	2949,
	3775,
	2949,
	2949,
	2949,
	2949,
	2949,
	2949,
	2949,
	2949,
	2949,
	2949,
	2949,
	2949,
	2949,
	2949,
	2949,
	2942,
	2949,
	2911,
	2940,
	2904,
	2904,
	2904,
	2904,
	2904,
	2949,
	2372,
	2442,
	756,
	2372,
	2949,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	53,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
