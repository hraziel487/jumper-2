using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using UnityEditor;

public class Pausa : MonoBehaviour
{
    public GameObject PanelPausa;
    public Button BotonSalir;

    private void Awake() {

        PanelPausa.SetActive(false);
        BotonSalir.onClick.AddListener(SalirJuego);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            PanelPausa.SetActive(true);
            Time.timeScale = 0;
        }
    }

    public void ContinuarJuego()
    {

        PanelPausa.SetActive(false);
        Time.timeScale = 1;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

    }

    private void SalirJuego()
    {
        print("Juego finalizado");
        Application.Quit();
      //  UnityEditor.EditorApplication.isPlaying = false;

    }

}
