using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VidaPlayer : MonoBehaviour
{
    public float vida = 100;
    public Image imagen;
    
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        vida = Mathf.Clamp(vida, 0, 100);
        imagen.fillAmount = vida / 100;

        if (vida <= 0)
        {
            SceneManager.LoadScene("GameOver", LoadSceneMode.Single);
            Debug.Log("El juego a terminado");
        }
    }
}
