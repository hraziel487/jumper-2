using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class ResetGame : MonoBehaviour
{
    
    public void Reiniciar()
    {
        SceneManager.LoadScene("Nivel 1"); // loads current scene
    }

}
